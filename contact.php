<?php
include("Mailjet/php-mailjet-v3-simple.class.php");
if($_POST)
{
    $to_email       = "hello@rajeshgurung.com"; //Recipient email, Replace with own email here
    
    //check if its an ajax request, exit if not
    if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
        
        $output = json_encode(array( //create JSON data
            'type'=>'error', 
            'text' => 'Sorry Request must be Ajax POST'
        ));
        die($output); //exit script outputting json data
    } 
    
    //Sanitize input data using PHP filter_var().
    $user_name      = filter_var($_POST["contact_name"], FILTER_SANITIZE_STRING);
    $user_email     = filter_var($_POST["contact_email"], FILTER_SANITIZE_EMAIL);
    $subject        = filter_var($_POST["contact_subject"], FILTER_SANITIZE_STRING);
    $message        = filter_var($_POST["contact_message"], FILTER_SANITIZE_STRING);
    
    //additional php validation
    if(strlen($user_name)<4){ // If length is less than 4 it will output JSON error.
        $output = json_encode(array('type'=>'error', 'text' => 'Name is too short or empty!'));
        die($output);
    }
    if(!filter_var($user_email, FILTER_VALIDATE_EMAIL)){ //email validation
        $output = json_encode(array('type'=>'error', 'text' => 'Please enter a valid email!'));
        die($output);
    }
    if(strlen($subject)<3){ //check emtpy subject
        $output = json_encode(array('type'=>'error', 'text' => 'Subject is required'));
        die($output);
    }
    if(strlen($message)<3){ //check emtpy message
        $output = json_encode(array('type'=>'error', 'text' => 'Too short message! Please enter something.'));
        die($output);
    }
    
    //email body
    $message_body = $message."\r\n\r\n-".$user_name."\r\nEmail : ".$user_email;
    
    //proceed with PHP email.
    $headers = 'From: '.$user_email.'' . "\r\n" .
    'Reply-To: '.$user_email.'' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();
    $apiKey = '83712306c45f29c7165098be9e314caf';
	$secretKey = '1a1a5b19d8b1e8959b2369b7cf36bcea';

	$mj = new Mailjet($apiKey, $secretKey);
	$params = array(
	"method" => "POST",
	"from" => "info@buxbyte.com <BuxByte Inc>",
	"to" => $to_email,
	"subject" => $subject,
	"text" => $message_body
	);

	$result = $mj->sendEmail($params);
    //$send_mail = mail($to_email, $subject, $message_body, $headers);
    
    if($mj->_response_code != 200)
    {
        //If mail couldn't be sent output error. Check your PHP email configuration (if it ever happens)
        $output = json_encode(array('type'=>'error', 'text' => 'Could not send mail! Please check your PHP mail configuration.'));
        die($output);
    }else{
        $output = json_encode(array('type'=>'message', 'text' => 'Hi '.$user_name .' Thank you for your email'));
        die($output);
    }
}
?>